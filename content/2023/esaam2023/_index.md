---
title: "eSAAM 2023 on Cloud-to-Edge Continuum"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2023 on Cloud-to-Edge Continuum</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        3rd Eclipse Security, AI, Architecture and Modelling Conference<br/>on Cloud to Edge Continuum
    </h2>
    <h3 class="featured-jumbotron-subtitle text-center" style="color: white;">October 17, 2023 | Ludwigsburg, Germany</h3>
custom_jumbotron_class: container-fluid
date: 2023-10-17T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
show_featured_footer: false
hide_call_for_action: true
header_wrapper_class: "header-esaam2023-event"
hide_breadcrumb: true
container: "container-fluid esaam-2023-event"
summary: "eSAAM 2023 will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, specifically  focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2023"
layout: single
main_menu: esaam2023  
keywords: ["eclipse", "UOM", "UPM", "conference", "research", "eSAAM", "SAAM", "Cloud", "Edge", "IoT", "Cloud-Edge-Continuum", "Edge-Cloud-Continuum", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling"]
links: [ [href: "#agenda", text: "Recordings"], [href: "#agenda", text: "Agenda"], [href: "#proceedings", text: "Proceedings"]  ]
---

[//]: # (Introduction)
{{< grid/section-container id="about" class="featured-section-row featured-section-row-light-bg text-center" isMarkdown="false">}}
	<h2>The 3rd Eclipse SAAM on Cloud to Edge Continuum 2023 is now over! Thank you for your interest and attendance!</h2> 
	<p align="center">
	The conference brought together industry experts and researchers working on innovative software and systems solutions for the next generation 
	of Cloud-to-Edge continuum, specifically focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software 
	Architecture, Modelling and related challenges. <br/> <br/>
	The event was co-located with 
	<a href="https://eclipsecon.org"><img src="images/EclipseCon-Logo-2023-Black.png" width="200"/></a>
	<p/><p align="center">
	This was a great opportunity to meet our dynamic <strong>open source community</strong>.</p>

{{</ grid/section-container >}}

[//]: # (Proceedings)
{{< grid/section-container id="proceedings" class="featured-section-row featured-section-row-highligthed-bg text-center">}}
	<h2>Proceedings</h2>
	<a href="https://dl.acm.org/doi/proceedings/10.1145/3624486"><img src="images/ACM_ICPS_v.2B.png" width="200"/></a><br/>
	<p>eSAAM 2023 proceedings are now on the 
	<a href="https://dl.acm.org/doi/proceedings/10.1145/3624486"><strong>ACM Digital Library</strong></a></p>
{{</ grid/section-container >}}

[//]: # (Indexed by)
{{< grid/section-container id="indexes" class="featured-section-row featured-section-row-light-bg text-center">}}
    <h2>Indexed by</h2>
	{{< grid/div class="row" isMarkdown="false">}}
	<!--
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Scopus](images/scopus_logo.png)](https://www.scopus.com/)
		{{</ grid/div >}}
	-->
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Google Scholar](images/GScholar_logo.png)](https://scholar.google.com/)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![dblp](images/dblp_logo.png)](https://dblp.org)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Semantic Scholar](images/semantic_scholar_logo.png)](https://www.semanticscholar.org)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![ORCID](images/ORCID-logo.png)](https://orcid.org/)
		{{</ grid/div >}}
	{{</ grid/div >}}
	<p>You can  retrieve the slides of the talks under the Agenda section</p>
{{</ grid/section-container >}}


[//]: # (Topics)
{{< grid/section-container id="topics" class="featured-section-row featured-section-row-highligthed-bg text-center">}}
	<h2>Technical topics of interest in Cloud Computing</h2>
	{{< grid/div class="row" isMarkdown="false">}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Security and Privacy for the Cloud to Edge Continuum](images/security-black.png)](topics/index.html#security-and-privacy-for-the-cloud-to-edge-continuum)
### [Security and Privacy](topics/index.html#security-and-privacy-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Artificial Intelligence for the Cloud to Edge Continuum](images/ai-black.png)](topics/index.html#artificial-intelligence-for-the-cloud-to-edge-continuum)
### [Artificial Intelligence](topics/index.html#artificial-intelligence-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Architectures for the Cloud to Edge Continuum](images/icon-architecture.png)](topics/index.html#architectures-for-the-cloud-to-edge-continuum)
### [Architecture](topics/index.html#architectures-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Modelling for the Cloud to Edge Continuum](images/modeling-black.png)](topics/index.html#modelling-for-the-cloud-to-edge-continuum)
### [Models and Services](topics/index.html#modelling-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
	{{</ grid/div >}}
{{</ grid/section-container >}}

[//]: # (Speakers)
{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-light-bg eclipsefdn-user-display-circle" >}}
  {{< events/esaam_user_display event="esaam2023" year="2023" title="Speakers" 
  		source="speakers" imageRoot="/2023/esaam2023/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

[//]: # (Agenda)
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-highligthed-bg" title="Agenda">}}
  {{< events/esaam_agenda event="esaam2023" year="2023" >}}
{{</ grid/section-container >}}

[//]: # (Best Paper Award)
{{< grid/section-container id="proceedings" class="featured-section-row featured-section-row-light-bg text-center">}}
	<h2>Best Paper Award</h2>
	<p>We are delighted to announce that the Technical Program Committee has awarded <br/>the eSAAM 2023 Best Paper Award to <strong>Savidu Dias and Ella Peltonen</strong> <br/>for their paper on <br/><i><strong>LinkEdge: Open-sourced MLOps Integration with IoT Edge</strong></i>.</p>
	<img src="eSAAM2023-Best Paper Award.png" width="800" alt="Best Paper Award"/>
	
{{</ grid/section-container >}}


[//]: # (TCP)
<!-- PROGRAM COMMITTEE --> 
{{< grid/section-container id="program-committee" class="featured-section-row featured-section-row-highligthed-bg">}}
<h2>Technical Program Committee</h2>

<p>The Technical Program Committee is an independent panel of expert volunteers and as such will do their best to judge papers objectively and on the principle of a level playing field for all. </p>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Apostolos  Ampatzoglou, University of Macedonia
* Vasilios  Andrikopoulos, University of Groningen
* Luca  Anselma, University of Turin
* Nuno Antunes, University of Coimbra
* Alessandra  Bagnato, SOFTEAM
* Rami  Bahsoon, University of Birmingham
* Peter  Bednar, TUKE
* Samira Briongos, NEC Laboratories Europe
* Benoit Combemale, University of Rennes
* Davide  Conzon , LINKS Foundation
* Gil  Gonçalves, FEUP
{{</ grid/div >}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Fulya Horozal, ATB
* Teodoro  Montanaro, Università del Salento
* Tero    Päivärinta, Luleå University of Technology
* Fabio Palomba, University of Salerno
* Panagiotis  Papadimitriou, University of Macedonia
* Ella Peltonen, University of Oulu
* Eliseu  Pereira, FEUP
* Sebastian Scholze, ATB
* Miltiadis Siavvas, CERTH
* Dimitris Syrivelis, NVIDIA, Israel
* Dimitrios Tsoukalas, CERTH
{{</ grid/div >}}
{{</ grid/div >}}

{{</ grid/section-container >}}

[//]: # (Organizing Committee)
{{< grid/section-container id="committee" class="featured-section-row text-center featured-section-row-light-bg eclipsefdn-user-display-circle" >}}
	{{< events/esaam_user_display event="esaam2023" year="2023" title="Organizing Committee" source="committee" imageRoot="/2023/esaam2023/images/speakers/" subpage="committee" displayLearnMore="false" />}}

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-8 padding-bottom-20" isMarkdown="false">}}
<a href="https://www.uom.gr/en"><img src="images/organizers/logo-uom.png" width="200" alt="University of Macedonia"/></a>
{{</ grid/div >}}

{{< grid/div class="col-md-8 padding-bottom-20" isMarkdown="false">}}
<a href="https://eclipse.org"><img src="images/organizers/logo-ecl.png" width="200" alt="Eclipse Foundation"/></a>
{{</ grid/div >}}

{{< grid/div class="col-md-8 padding-bottom-20" isMarkdown="false">}}
<a href="https://eclipse.org"><img src="images/organizers/logo-upm.png" width="200" alt="Universidad Politecnica de Madrid"/></a>
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}

[//]: # (Sponsors)
{{< grid/section-container id="sponsors" class="featured-section-row featured-section-row-highligthed-bg text-center">}}
  {{< events/sponsors event="esaam2023" year="2023" source="sponsors" title="Our Sponsors" useMax="false" displayBecomeSponsor="false">}}
{{</ grid/section-container >}}

[//]: # (Previous Conferences)
{{< grid/section-container id="previous-esaam" class="featured-section-row featured-section-row-light-bg" isMarkdown="true">}}
## Previous conferences
* [eSAAM on Mobility 2021](https://events.eclipse.org/2021/saam-mobility/)
* [eSAM IoT 2020](https://events.eclipse.org/2020/sam-iot/)

{{</ grid/section-container >}}

{{< grid/section-container id="resources" class="featured-section-row featured-section-row-highligthed-bg" isMarkdown="true">}}
## Images from
* [Header by Roman from Pixabay](https://pixabay.com/illustrations/cloud-computer-circuit-board-cpu-6532831/)
* [Security and Privacy icon by F.Adiima from NounProject](https://thenounproject.com/search/?q=security&i=3349833)
* [Artificial Intelligence icon by priyanka from NounProject](https://thenounproject.com/search/?q=artificial%20intelligence&i=2858867)
* [Architecture icon by SBTS from NounProject](https://thenounproject.com/icon/web-architecture-1711291)
* [Models and Services icon by G.Tachfin from NounProject](https://thenounproject.com/search/?q=modelling&i=2710243)
{{</ grid/section-container >}}


{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

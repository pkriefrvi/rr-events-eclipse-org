---
title: "Committee Members"
seo_title: TheiaCon 2022 Committee Members
date: 2022-09-19T13:37:19-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
hide_sidebar: true
hide_breadcrumb: true
layout: program-committee
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---

{{< events/user_bios event="theiacon" year="2022" source="committee" imgRoot="/2022/theiacon/images/committee/" >}}